package Network;

import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) throws IOException {
        //System.out.println("Старт сервера");
        BufferedReader in = null;
        PrintWriter    out= null;

        ServerSocket server = null; //слушащий
        Socket       client = null; // для работы с клиентом

        // создаем серверный сокет
        try {
            server = new ServerSocket(1234); // знать Ip адресс
        } catch (IOException e) {
            //System.out.println("Ошибка связывания с портом 1234");
            System.exit(-1);
        }

        try {
            //System.out.print("Ждем соединения");
            client = server.accept(); // вызываем метод который содержит ожидание
            //System.out.println("Клиент подключился");
        } catch (IOException e) {
            //System.out.println("Не могу установить соединение");
            System.exit(-1);
        }

        in  = new BufferedReader( new InputStreamReader(client.getInputStream())); //запрашиваем у клиента поток ввода
        out = new PrintWriter(client.getOutputStream(),true); // и поток вывода
        String input,output;

        //System.out.println("Ожидаем сообщений");


        while ((input = in.readLine()) != null) {
            //if (input.equalsIgnoreCase("exit")) // если клиент написал exit то происходит разрыв соединения
            //    break;
            ServerTime new_time = new ServerTime();
            ServerAphorism new_aphorism = new ServerAphorism();
            ServerMessages new_messages = new ServerMessages();
            ServerCurrency new_currency = new ServerCurrency();

            // 1. Получаем от клиента запрос о текущем времени на сервере, отправдяем ему ответ
            if (input.equals("time")) {
                out.println("Текущее время сервера: "
                        + new_time.getHours() + ":"
                        + new_time.getMinute() + ":"
                        + new_time.getSec());
            }

            // 2. Получаем от клиента запрос на афоризм, отправляем ему с сервера афоризм
            if (input.equals("aphorism")) {
                out.println(new_aphorism.getAphorism());
            }

            // 3. Получаем от клиента его логин (он хочет получить задание)
            // если логин совпадает и на это время есть сообщение, то отправляем ему сообщение
            if (input.equals("myLogin")) {
                new_messages.setLogin(input);
                out.println(new_messages.getMessages());
            }

            // 4. Получаем от клиента запрос курса валют, отправляем ему курс валют
            if (input.equals("currency")) {
                out.println(new_currency.getCurrency());
            }

    //System.out.println(input); // выведет на экране сервера
}
        out.close();
        in.close();
        client.close();
        server.close();
    }
}