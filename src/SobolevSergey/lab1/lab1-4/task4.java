/* Лабораторный практикум №1. Задача 4. Свертка числовых диапазонов
Написать программу, осуществляющую свертку числовых диапазонов (обратная задача к 3).
Например:  1,2,4,5,6,7,18,19,20,21.. -> ''1,2,4-7,18-21''

*/

public class task4{
	public static void main(String[] args){
		char string[] = args[0].toCharArray();
		int[] digits = new int[string.length];
		int i,j,temp;
		
		for(i=0,j=0;i<string.length;i++,j++){
			int prev=0;
			while(i<string.length && string[i]!=','){
				temp = Character.digit(string[i],10); 
				prev = temp + prev*10;
				i++;
			}
			digits[j]=prev;
		}
		
		for(i=0;i<j-2;i++){
			while(digits[i+2]-digits[i]>=3){
				System.out.print(digits[i] + "," + digits[i+1] + ",");
				i=i+2;
			}
			temp=i;
			while(digits[i+1]-digits[i]==1){
				i++;				
			}
			System.out.print(digits[temp] + "-" + digits[i]);
			if(i<j-3)
				System.out.print(",");
		}
	}
}