/*Задача 6. Проект CircleDemo

Разработать класс Circle (круг), содержащий следующие поля:

Radius - радиус
Ference - длина окружности
Area - площадь круга
Данные поля рекомендуется объявить как переменные типа double и с 
модификатором доступа private для безопасности. Доступ к этим полям 
следует ограничить с помощью методов класса.

при установке значения радиуса пересчитывать длину окружности и пощадь;
при установке длины окружности пересчитывать радиус и площадь;
при установке площади пересчитывать радиус и длину окружности.
При помощи класса Circle решить две вспомогательные задачи:

"Земля и верёвка"
Решить с использованием класса Circle следующую задачу:
Представим, что земля имеет форму идеального шара. 
Вокруг поверхности земного шара туго натянута верёвка 
(между ней и поверхностью нет никакого зазора). 
Кто-то разрезает верёвку в произвольном месте и добавляет 
кусок верёвки длиной 1 метр. После вставки между 
поверхностью земли и верёвкой возникает зазор, вызванный 
увеличением длины. Найти величину этого зазора. Примем 
за радиус земли расстояние в 6378.1 км

"Бассейн"
Решить с помощью класса Circle следующую задачу:
Необходимо рассчитать стоимость материалов для бетонной дорожки вокруг 
круглого бассейна, а также стоимость материалов ограды вокруг бассейна
(вместе с дорожкой). Стоимость 1 квадратного метра бетонного покрытия 
1000 р. Стоимость 1 погонного метра ограды 2000 р. Радиус бассейна 3 м. 
Ширина бетонной дорожки вокруг бассейна 1 м.

*/
class Circle
{
    private double Radius;
    private double Ference;
    private double Area;

    void setRadius(double r){
        Radius = r;
        Ference = 2 * Math.PI * Radius;
        Area = Math.PI * Radius * Radius;
    }
	void setFerence(double f){
        Ference = f;
        Radius = Ference / (Math.PI * 2);
        Area = Math.PI * Radius * Radius;
    }
    void setArea(double a){
        Area = a;
        Radius = Math.sqrt(a / Math.PI);
        Ference = 2 * Math.PI * Radius;
    }
    double getRadius(){
        return Radius;
    }
	double getFerence(){
        return Ference;
    }
    double getArea(){
        return Area;
    }
}

public class task6{
    public static void main(String args[]){
/* *********** "Земля и верёвка" *********** */
		Circle earth = new Circle();
		earth.setRadius(6378.1 * 1000);
        earth.setFerence(earth.getFerence() + 1);
        System.out.println("Clearance = " + (earth.getRadius() - 6378.1 * 1000) + " meters");

/* *********** "Бассейн" ******************* */
		Circle pool = new Circle();
        pool.setRadius(3);
        Circle roadPool = new Circle();
        roadPool.setRadius(3 + 1);
		double priceTrack = (roadPool.getArea() - pool.getArea()) * 1000.0;
		System.out.println("Price track: " + priceTrack + "rub.");
		double priceFence = roadPool.getFerence() * 2000.0;
		System.out.println("Price track: " + priceFence + "rub.");
		System.out.printf("Total price: %.2f rub.", priceTrack+priceFence);
	}
}


