package Dekanat;
import java.awt.*;
/**
 * Created by SKS on 02.04.2017.
 */
/*
* Разработать класс Group для хранения информации об учебной группе
Примерный перечень полей:
Title - название группы
Students - массив из ссылок на студентов
Num - количество студентов в группе
Head - ссылка на старосту (из членов группы)

Обеспечить класс следующими методами:
создание группы с указанием названия
добавление студента
избрание старосты
поиск студента по ФИО или ИД
вычисление среднего балла в группе
исключение студента из группы
* */
public class Group {

    private String Title;                               // Title - название группы
    Student [] students = new Student[30];              // Students - массив из ссылок на студентов
    private int Num;                                    // Num - количество студентов в группе
    private Student Head;                               // Head - ссылка на старосту (из членов группы)

    //Обеспечить класс следующими методами: ********************************************************
    // создание группы с указанием названия
    public Group(String Title){
        this.Title = Title;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getNum() {
        return Num;
    }

    // добавление студента
    public void addStudent(Student student) throws ArrayIndexOutOfBoundsException{
        students[Num]=student;
        Num++;
    }

    //избрание старосты
    public String LeaderGroup(){
        double maxRating=0;
        for(int i=0; i<Num; i++){
            if(students[i].CalculatingAverageRatingStudent()>maxRating){
                maxRating=students[i].CalculatingAverageRatingStudent();
                Head=students[i];
            }
        }
        return "Для группы " + Head.getGroup().getTitle() + " назначен староста " + Head.getFio();
    }

    //поиск студента по ФИО или ИД
    //поиск студента по ID
    private String searchStudentID(int id) {
        Student foundStudent=null;
        String message="";
        for(int i=0; i<Num; i++){
            if(students[i].getID()==id){
                foundStudent = students[i];
                message = "Найден студент: "+foundStudent.getID()+" "+foundStudent.getFio()+" "+foundStudent.getGroup().getTitle();
                break;
            }
            else message = "Студент "+id+" не найден!";
        }
        return message;
    }

    //поиск студента по ФИО
    private String searchStudentFio(String fio) {
        Student foundStudent=null;
        String message="";
        for(int i=0; i<Num; i++){
            if(students[i].getFio().equals(fio)){
                foundStudent = students[i];
                message = "Найден студент: "+foundStudent.getID()+" "+foundStudent.getFio()+" "+foundStudent.getGroup().getTitle();
                break;
            }
            else message = "Студент "+fio+" не найден!";
        }
        return message;
    }

    //вычисление среднего балла в группе
    public double calculationAverageScoreGroup(){
        double temp = 0.0;
        for (int i=0; i<Num; i++)
            temp +=students[i].CalculatingAverageRatingStudent();

        return (double)temp/Num;
    }

    //исключение студента из группы
    public void deleteStudentFromGroup(Student student){
        Student[] temp = new Student[Num];                  // временный массив
        int newNum = 0;                                     // новое кол-во студентов в группе
        for(int i=0; i<Num; i++){
            if(!(students[i].equals(student))){
                temp[newNum] = students[i];
                newNum++;
            }
        }
        for(int j=0; j<Num; j++)                            // очищаем массив от записей
            students[j] = null;

        for(int k=0; k<Num-1; k++)                          // записываем новый массив ссылок на студентов
            students[k]=temp[k];

        Num--;                                              // меняем кол-во студентов в группе
    }
}
